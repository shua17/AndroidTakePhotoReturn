package lxj.ms.com.photodmo.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import lxj.ms.com.photodmo.R;

/**
 * 适配器
 */

public class MyAdapter extends BaseAdapter {
    private Context context;
    private List<Bitmap> list;

    public MyAdapter(Context context, List<Bitmap> list) {
        this.context = context;
        this.list = list;
    }

    public void setList(List<Bitmap> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder myViewHolder = null;
        if (convertView == null) {
            myViewHolder = new MyViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_layout, null);
            myViewHolder.iv_view = convertView.findViewById(R.id.iv_view);
            convertView.setTag(myViewHolder);
        } else {
            myViewHolder = (MyViewHolder) convertView.getTag();
        }

        if (list.isEmpty()) {
            myViewHolder.iv_view.setImageResource(R.mipmap.ic_add);
        } else if (position < list.size()) {
            myViewHolder.iv_view.setImageBitmap(list.get(position));
        } else {
            myViewHolder.iv_view.setImageResource(R.mipmap.ic_add);
        }

        return convertView;
    }

    class MyViewHolder {
        ImageView iv_view;
    }
}
