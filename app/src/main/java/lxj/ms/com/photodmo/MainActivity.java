package lxj.ms.com.photodmo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lxj.ms.com.photodmo.adapter.MyAdapter;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private MyAdapter myAdapter;
    private GridView mgridView;

    private List<Bitmap> listData = new ArrayList<>();

    private String path = Environment.getExternalStorageDirectory()
            + File.separator + "MyApp" + File.separator + "image" + File.separator;
    private String fileName;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        mgridView = findViewById(R.id.grid_view);
        myAdapter = new MyAdapter(MainActivity.this, listData);
        mgridView.setAdapter(myAdapter);
        mgridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position + 1 == myAdapter.getCount()) {
                    checkPermissionGranted();
                } else {
                    Toast.makeText(MainActivity.this, "您点击了 ： " + position, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void takePhotoBy8(int requestCode) {
        fileName = String.valueOf(System.currentTimeMillis()) + ".jpg";//文件的名字

        File file = new File(path);
        if (!file.exists()) file.mkdirs();//注意这里时 mkdirs,mkdir 会导致resultCode一直等于0

        File imgFile = new File(path, fileName);//生成该文件的 File 对象
        Uri imgUri;

        if (Build.VERSION.SDK_INT < 24) {
            imgUri = Uri.fromFile(imgFile);
        } else {
            // 注意：必须在 AndroidManifest.xml 添加对应的 provider，
            // 这里的第二个参数必须和 AndroidManifest 中 provider里面的 authorities: = 一致
            imgUri = FileProvider.getUriForFile(this, "lxj.ms.com.photodmo.provider", imgFile);
            //System.out.println(imgUri);
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001) {
            if (resultCode == Activity.RESULT_OK) {
                bitmap = BitmapFactory.decodeFile(path + fileName, getBitmapOption(5));
                // 将拍到的照片存放于集合中
                listData.add(bitmap);
                myAdapter.setList(listData);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 图片压缩
     */
    private BitmapFactory.Options getBitmapOption(int inSampleSize) {
        System.gc();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inSampleSize = inSampleSize;
        return options;
    }

    @AfterPermissionGranted(10)
    private void checkPermissionGranted() {
        String[] a = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, a)) {
            takePhotoBy8(1001);
        } else {
            EasyPermissions.requestPermissions(this, "xx", 10, a);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }
}
